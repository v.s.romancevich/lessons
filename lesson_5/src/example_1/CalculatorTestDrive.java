package example_1;

public class CalculatorTestDrive {
    public static void main(String[] args) {

        Calculatоr calculatоr = new Calculatоr();

        int x = 8;
        int y = 2;

        System.out.println("Сумма " + x + " и " + y
                + " равна " + calculatоr.sum(x, y));

        System.out.println("Расность " + x + " и " + y
                + " равна " + calculatоr.diff(x, y));

        System.out.println("Произведение " + x + " и " + y
                + " равна " + calculatоr.mult(x, y));

        System.out.println("Частное " + x + " и " + y
                + " равна " + calculatоr.div(x, y));
    }
}
