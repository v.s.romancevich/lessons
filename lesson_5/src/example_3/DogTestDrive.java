package example_3;

public class DogTestDrive {
    public static void main(String[] args) {
        Dog myDog = new Dog();

        myDog.setName("Шуруповерт");
        myDog.setAge(5);

        System.out.println("Мою собаку зовут " + myDog.getName());
        System.out.println("Ей " + myDog.getAge() + " лет");
    }
}
