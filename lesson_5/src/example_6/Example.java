package example_6;

public class Example {
    public static void main(String[] args) {
        int a = 3;
        byte b = 3;

        System.out.println("Пример № 1");
        System.out.println(a == b);

        String oneName = "Вова";
        String twoName = "Петя";
        String threeName = "Вова";

        System.out.println("Пример № 2");
        System.out.println(oneName == twoName);
        System.out.println(oneName == threeName);

        System.out.println("Пример № 3");
        Dog one = new Dog();
        one.setName("Шуруповерт");
        one.setAge(3);

        Dog two = new Dog();
        two.setName("Шуруповерт");
        two.setAge(3);

        Dog three = one;

        System.out.println(one == two);
        System.out.println(one == three);
        System.out.println(two == three);

        System.out.println("Пример № 4");

        System.out.println(one.equals(two));
        System.out.println(one.equals(three));
        System.out.println(two.equals(three));
    }
}
