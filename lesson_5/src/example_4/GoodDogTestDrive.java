package example_4;

public class GoodDogTestDrive {
    public static void main(String[] args) {
        GoodDog one = new GoodDog();
        one.setName("Бетмен");
        one.setSize(70);

        GoodDog two = new GoodDog();
        two.setName("Терминатор");
        two.setSize(8);

        one.bark();
        two.bark();
    }
}
