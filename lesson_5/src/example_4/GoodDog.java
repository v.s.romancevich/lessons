package example_4;

public class GoodDog {
    private String name;
    private int size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void bark() {
        if (size > 60) {
            System.out.println(name + " сказал Гав! Гав!");
        } else if (size > 14) {
            System.out.println(name + " сказал Ваф! Ваф!");
        } else {
            System.out.println(name + " сказал Тяв! Тяв!");
        }
    }
}
