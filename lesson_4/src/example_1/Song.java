package example_1;

public class Song {
    String title;
    String artist;

    void play() {
        System.out.println("Проигрывается: песня \"" + title + "\", исполнитель \""
                + artist + "\".");
    }
}
