package example_2;

public class Alarm {
    // создаем переменную экземпляра
    int alarmTime;      // время срабатывания будильника

    // метод который устанавливает значение переменной alarmTime
    // сеттер
    void setAlarmTime(int alarmTime) {
        this.alarmTime = alarmTime;
    }

    // метод который читает значение переменной alarmTime
    // геттер
    int getAlarmTime() {
        return alarmTime;
    }
}
