package example_2;

public class AlarmTestDrive {
    public static void main(String[] args) {
        // создаем объект класса Alarm
        Alarm alarm = new Alarm();

        // установим значение 5 в
        // переменную экземпляра класса
        // при помощи метода setAlarmTime()
        alarm.setAlarmTime(5);

        // прочитаем значение переменной экземпляра класса
        // при помощи метода getAlarmTime()
        System.out.println(alarm.getAlarmTime());

    }
}
