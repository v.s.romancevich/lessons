package example_1;

public class TigerTestDrive {
    public static void main(String[] args) {
        // создаем новый объект
        // объект класса tiger
        Tiger tiger = new Tiger();

        // используем оператор доступа (.)
        // устанавливаем значения "Амур" и 5
        tiger.name = "Амур";
        tiger.age = 5;

        System.out.println("Нашего тигра зовут " + tiger.name);
        System.out.println("Возраст нашего тигра  " + tiger.age + " лет.");
        System.out.print("Тигр рычит: ");
        // вызываем метод voice()
        tiger.voice();
    }
}
