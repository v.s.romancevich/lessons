package example_4;

public class GameLauncher {
    public static void main(String[] args) {
        // создаем объект класса GuessGame
        GuessGame game = new GuessGame();
        // вызываем метод startGame()
        game.startGame();
    }
}
