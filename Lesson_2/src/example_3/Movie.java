package example_3;

public class Movie {
    // переменные экземпляра
    String title;   // название фильма
    String genre;   // жанр фильма
    int rating;     // рейтинг фильма

    // метод
    void playIt() {
        System.out.println("Проигрывается фильм " + title);
    }
}
