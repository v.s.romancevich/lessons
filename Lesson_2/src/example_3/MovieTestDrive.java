package example_3;

public class MovieTestDrive {
    public static void main(String[] args) {
        // создадим первый объект
        Movie oneMovie = new Movie();
        // установим значения для его переменных экземпляра
        // состояние
        oneMovie.title = "Титаник";
        oneMovie.genre = "драма";
        oneMovie.rating = 9;

        // создадим второй объект
        Movie twoMovie = new Movie();
        // установим значения для его переменных экземпляра
        // состояние
        twoMovie.title = "Аватар";
        twoMovie.genre = "фантастика";
        twoMovie.rating = 9;

        // создадим третий объект
        Movie threeMovie = new Movie();
        // установим значения для его переменных экземпляра
        // состояние
        threeMovie.title = "Матрица";
        threeMovie.genre = "фантастика";
        threeMovie.rating = 9;

        // вызовем методы наших объектов
        oneMovie.playIt();
        twoMovie.playIt();
        threeMovie.playIt();
    }
}
