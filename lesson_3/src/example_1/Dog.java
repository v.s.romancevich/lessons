package example_1;

public class Dog {
    String name;

    public static void main(String[] args) {
        // Создаем объект Dog и получаем к нему доступ
        Dog dog1 = new Dog();
        dog1.bark();
        dog1.name = "Дружок";
        // Теперь создадим массив типа Dog
        Dog[] myDog = new Dog[3];
        // и поместим в него несколько элементов
        myDog[0] = new Dog();
        myDog[1] = new Dog();
        myDog[2] = dog1;
        // Теперь получим доступ к объектам Dog
        // с помощью ссылок из массива
        myDog[0].name = "Шарик";
        myDog[1].name = "Пират";
        // Узнаем имя у myDog[2]
        System.out.print("Имя последней собаки - ");
        System.out.println(myDog[2].name);
        // Переберем все элементы массива
        // и вызовем для каждого метод bark()
        int x = 0;
        while (x < myDog.length) {
            myDog[x].bark();
            x = x + 1;
        }
    }

    private void bark() {
        System.out.println(name + " сказал Гав!");
    }
}
