package example_1;

public class IntArray {
    public static void main(String[] args) {
        // создание и инициализация массива - способ 1
        // int[] nums = {3, 4, 5, 6};
        // создание и нициализация массива - способ 2
        int[] nums = new int[5];

        // первый способ
        nums[0] = 6;
        nums[1] = 19;
        nums[2] = 44;
        nums[3] = 42;
        nums[4] = 10;
        // второй способ
        int x = 0;
        while (x < nums.length) {
            nums[x] = (int) (Math.random() * 100);
            x = x + 1;
        }
        // первый способ вывода в консоль
        System.out.println("Превый способ:");
        System.out.println("nums[0] = " + nums[0]);
        System.out.println("nums[1] = " + nums[1]);
        System.out.println("nums[2] = " + nums[2]);
        System.out.println("nums[3] = " + nums[3]);
        System.out.println("nums[4] = " + nums[4]);
        // второй способ вывода в консоль
        System.out.println();
        System.out.println("Второй способ:");
        int y = 0;
        while (y < nums.length) {
            System.out.println("nums[" + y + "] = " + nums[y]);
            y = y + 1;
        }
    }
}
